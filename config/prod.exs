use Mix.Config

config :eventrist, Eventrist.Endpoint,
  http: [port: 4000],
  url: [scheme: "https", host: System.get_env("PHOENIX_HOSTNAME"), port: 443],
  cache_static_manifest: "priv/static/manifest.json",
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  server: true

# Do not print debug messages in production
config :logger, level: :info

# Configure your database
config :eventrist, Eventrist.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: System.get_env("DATABASE_URL"),
  ssl: true,
  pool_size: 20

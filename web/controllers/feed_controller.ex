defmodule Eventrist.FeedController do
  use Eventrist.Web, :controller
  import Eventrist.EventsController, only: [user_events: 1]

  def feed(conn, params) do
    user_id = params["user_id"]
    key = params["key"]

    if key == nil do
      conn
      |> put_status(400)
      |> json(%{msg: "key cannot be null"})
    else
      calendar_key = Eventrist.Repo.get_by(Eventrist.CalendarKey, user_id: user_id, key: key)
      if calendar_key == nil do
        conn
        |> put_status(:unauthorized)
        |> json(%{msg: "invalid key"})
      else
        q = from event in user_events(user_id), preload: [:venue]
        events = Eventrist.Repo.all(q)
        conn
        |> put_layout(:none)
        |> put_resp_content_type("text/calendar")
        |> put_resp_header("cache-control", "no-store, no-cache, must-revalidate")
        |> put_resp_header("pragma", "no-cache")
        |> render "feed.ical", events: events
      end
    end
  end
end

defmodule Eventrist.RegistrationController do
  use Eventrist.Web, :controller
  import Eventrist.Session

  alias Eventrist.User

  def login(conn, _params) do
    if logged_in?(conn) do
      redirect conn, to: events_path(conn, :next_event)
    else
      changeset = User.changeset(%User{})
      render conn, changeset: changeset
    end
  end

  def signup(conn, _params) do
    if logged_in?(conn) do
      redirect conn, to: events_path(conn, :next_event)
    else
      changeset = User.changeset(%User{})
      render conn, changeset: changeset
    end
  end

  def do_login(conn, %{"user" => user_params}) do
    case Eventrist.User.login(user_params, Eventrist.Repo) do
      {:ok, user} ->
        conn
        |> put_session(:current_user, user.id)
        |> redirect(to: events_path(conn, :next_event))
      :error ->
        changeset = User.changeset(%User{}, user_params)
        conn
        |> put_flash(:error, "Wrong email or password")
        |> render("login.html", changeset: changeset)
    end
  end

  def do_signup(conn, %{"user" => user_params}) do
    changeset = User.changeset(%User{}, user_params)

    case Eventrist.Repo.insert(changeset) do
      {:ok, model} ->
          conn
          |> put_session(:current_user, model.id)
          |> redirect(to: events_path(conn, :next_event))
      {:error, changeset} ->
          conn
          |> render("signup.html", changeset: changeset)
    end
  end

  def logout(conn, _) do
    conn
      |> delete_session(:current_user)
      |> redirect(to: registration_path(conn, :login))
  end

  defp resolve_user(conn) do
    case get_session(conn, :current_user) do
      id when id != nil ->
        user = Eventrist.User.get(id, Eventrist.Repo)
        assign(conn, :user, user)
      _ -> conn
    end
  end
end

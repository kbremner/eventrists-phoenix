defmodule Eventrist.EventsController do
  use Eventrist.Web, :controller

  @months_before_event 1

  def user_events(user_id) do
    user_events(user_id, "added")
  end

  def user_events(user_id, status) do
    now = Ecto.DateTime.utc

    from event in Eventrist.Event,
    join: saved in Eventrist.UserEvent,
      on: saved.status == ^status
        and saved.user_id == ^user_id
        and saved.event_id == event.id
        and event.start_time > ^now
        and not event.cancelled,
    select: event,
    order_by: [asc: event.start_time]
  end

  # get saved events for a user
  def index(conn, _params) do
    user_id = get_session(conn, :current_user)
    q = user_events(user_id)
    events = Eventrist.Repo.all(q)

    # okay to lookup this key here by user ID, as the user has to be
    # logged in to access this route
    calendar_key =
      case Eventrist.Repo.get_by(Eventrist.CalendarKey, user_id: user_id) do
        nil -> Eventrist.CalendarKey.create!(user_id)
        x   -> x
      end

    render conn, events: events, user_id: user_id, calendar_key: calendar_key.key
  end

  def ignored(conn, _params) do
    user_id = get_session(conn, :current_user)
    q = user_events(user_id, "ignored")
    events = Eventrist.Repo.all(q)

    render conn, events: events, user_id: user_id
  end

  def show(conn, params) do
    user_id = get_session(conn, :current_user)

    user_event_status = Eventrist.Repo.one(
      from user_event in Eventrist.UserEvent,
      where: user_event.user_id == ^user_id and user_event.event_id == ^params["id"],
      select: user_event.status
    )

    event = Eventrist.Repo.get!(Eventrist.Event, params["id"])
    |> Eventrist.Repo.preload(:venue)

    render conn, event: event, saved_status: user_event_status
  end

  def next_event(conn, _params) do
    user_id = get_session(conn, :current_user)
    now = Ecto.DateTime.utc

    upcoming_events =
      from event in Eventrist.Event,
      where: event.start_time > ^now
      and event.start_time < datetime_add(^now, @months_before_event, "month")
      and not event.cancelled

    not_saved_events =
      from event in upcoming_events,
        left_join: user_event in Eventrist.UserEvent,
        on: user_event.user_id == ^user_id and user_event.event_id == event.id,
       where: is_nil(user_event.id),
       select: event,
       order_by: [asc: event.start_time],
       limit: 1,
       preload: [:venue]

    event = Eventrist.Repo.one(not_saved_events)
    if event == nil do
      render conn, "no_next.html"
    else
      # want user to be able to copy link, so redirect instead of rendering content directly
      redirect conn, to: events_path(conn, :show, event.id)
    end
  end

  def save_event(conn, params) do
    event_id = params["id"]
    user_id = get_session(conn, :current_user)
    status = params["action"]
    save_event(event_id, user_id, status)

    path = case params["redirect_path"] do
      nil -> events_path(conn, :next_event)
      p   -> p
    end

    redirect conn, to: path
  end

  defp save_event(event_id, user_id, status) do
    event_changes = %{
      event_id:   event_id,
      user_id:    user_id,
      status:     status
    }
    case Eventrist.Repo.get_by(Eventrist.UserEvent, user_id: user_id, event_id: event_id) do
      nil ->
        changeset = Eventrist.UserEvent.changeset(%Eventrist.UserEvent{}, event_changes)
        Eventrist.Repo.insert!(changeset)
      x   ->
        changeset = Eventrist.UserEvent.changeset(x, event_changes)
        Eventrist.Repo.update!(changeset)
    end
  end
end

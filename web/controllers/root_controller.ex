defmodule Eventrist.RootController do
  use Eventrist.Web, :controller

  def index(conn, _params) do
    render conn
  end
end

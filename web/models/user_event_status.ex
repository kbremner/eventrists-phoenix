defmodule Eventrist.UserEventStatus do
  @behaviour Ecto.Type
  def type, do: :integer

  def cast("added") do
    {:ok, 0}
  end

  def cast("ignored") do
    {:ok, 1}
  end

  def cast(_), do: :error

  def load(0) do
    {:ok, "added"}
  end

  def load(1) do
    {:ok, "ignored"}
  end

  def load(_), do: :error

  def dump(integer) when is_integer(integer), do: {:ok, integer}
  def dump(_), do: :error
end

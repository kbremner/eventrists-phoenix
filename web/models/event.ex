defmodule Eventrist.Event do
  use Eventrist.Web, :model

  schema "events" do
    field :source, :string
    field :source_id, :string
    field :title, :string
    field :description, :string
    field :link, :string
    field :organiser, :string
    field :cancelled, :boolean
    field :start_time, Ecto.DateTime
    field :end_time, Ecto.DateTime
    belongs_to :venue, Eventrist.Venue
    has_many :user_events, Eventrist.UserEvent
    has_many :saved_by_users, through: [:user_events, :event]
    timestamps
  end

  @required_fields ~w(source source_id title description link organiser cancelled start_time venue_id)
  @optional_fields ~w(end_time)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:source_id, name: :events_source_source_id_index)
  end

  def find_by_organiser_start_time_and_city(organiser, start_time, city) do
    from event in Eventrist.Event,
    join: venue in Eventrist.Venue,
      on: event.venue_id == venue.id
        and venue.city == ^city
        and event.organiser == ^organiser
        and event.start_time == ^start_time,
        select: event
  end
end

defmodule Eventrist.CalendarKey do
  use Eventrist.Web, :model

  schema "calendar_keys" do
    field :key, :string
    belongs_to :user, Eventrist.User
    timestamps
  end

  @required_fields ~w(key user_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:user_id)
  end

  def create!(user_id) do
    changeset = changeset(%Eventrist.CalendarKey{}, %{ user_id: user_id, key: random_string(40)})
    Eventrist.Repo.insert!(changeset)
  end

  defp random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
  end
end
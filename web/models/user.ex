defmodule Eventrist.User do
  use Eventrist.Web, :model

  schema "users" do
    field :email, :string
    field :crypted_password, :string
    field :password, :string, virtual: true
    has_many :user_events, Eventrist.UserEvent
    has_many :saved_events, through: [:user_events, :user]
    has_one :calendar_key, Eventrist.CalendarKey
    timestamps
  end

  @required_fields ~w(email password)
  @optional_fields ~w()
  @min_password_len 5

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> update_change(:email, &String.downcase/1)
    |> update_password_hash
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: @min_password_len)
    |> unique_constraint(:email)
  end

  def login(params, repo) do
    user = repo.get_by(__MODULE__, email: String.downcase(params["email"]))
    case authenticate(user, params["password"]) do
      true -> {:ok, user}
      _    -> :error
    end
  end

  def get(id, repo) do
    Repo.get!(__MODULE__, id)
  end

  defp authenticate(user, password) do
    case user do
      nil -> false
      _   -> Comeonin.Bcrypt.checkpw(password, user.crypted_password)
    end
  end

  defp hashed_password(password) do
    Comeonin.Bcrypt.hashpwsalt(password)
  end

  defp update_password_hash(changeset) do
    case get_change(changeset, :password) do
      x when x != nil -> put_change(changeset, :crypted_password, hashed_password(x))
      _ -> changeset
    end
  end
end

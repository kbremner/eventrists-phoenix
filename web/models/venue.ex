defmodule Eventrist.Venue do
  use Eventrist.Web, :model

  schema "venues" do
    field :name, :string
    field :description, :string
    field :lat, :float
    field :lng, :float
    field :address, :string
    field :city, :string
    has_many :events, Eventrist.Event
    timestamps
  end

  @required_fields ~w(name lat lng city)
  @optional_fields ~w(description address)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:name)
  end

end

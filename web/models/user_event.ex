defmodule Eventrist.UserEvent do
  use Eventrist.Web, :model

  schema "user_events" do
    field :status, Eventrist.UserEventStatus
    belongs_to :event, Eventrist.Event
    belongs_to :user, Eventrist.User
    timestamps
  end

  @required_fields ~w(status event_id user_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> unique_constraint(:event_id, name: :user_events_user_id_event_id_index)
  end
end

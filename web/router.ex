defmodule Eventrist.Router do
  use Eventrist.Web, :router
  import Eventrist.Session

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :user_data do
    plug :require_login
  end

  pipeline :feed do
    plug :accepts, ["text/calendar"]
  end

  scope "/", Eventrist do
    pipe_through :browser

    get  "/login",  RegistrationController, :login
    post "/login",  RegistrationController, :do_login
    get  "/signup", RegistrationController, :signup
    post "/signup", RegistrationController, :do_signup
    get  "/logout", RegistrationController, :logout

    get "/loaderio-88ec75cd570841f1ac0b26dfd6efbb93", LoaderioController, :verify

    # main page is info about the app
    get "/", RootController, :index
  end

  scope "/", Eventrist do
    pipe_through :feed
    get "/users/:user_id/feed.ical", FeedController, :feed
  end

  scope "/", Eventrist do
    pipe_through [:browser, :user_data]

    # Next recommended event for logged in user
    get "/events/next", EventsController, :next_event
    # Get all events that the logged in user has ignored
    get "/events/ignored", EventsController, :ignored
    # Save an event for the logged in user, either ignoring or adding it
    post "/events/:id/save", EventsController, :save_event
    # Get all events for the logged in user and show details about a specific event
    resources "/events", EventsController, only: [:index, :show]
  end

  defp require_login(conn, _) do
    case !logged_in?(conn) do
      true  -> redirect(conn, to: Eventrist.Router.Helpers.registration_path(conn, :login)) |> halt
      _     -> conn
    end
  end

  #pipeline :review_checks do
  #  plug :ensure_authenticated_user
  #  plug :ensure_user_owns_review
  #end
  #scope "/reviews", Eventrist do
  #  pipe_through :review_checks
  #  resources "reviews", ReviewController
  #end

  # see info for a particular user and see the events for that user
  #resources "users", UserController, only: [:show] do
  #  resources "events", EventsController, only: [:index, :create, :delete]
  #  resources "tags", TagsController, only: [:index]
  #end

  # see organisers and their events
  #resources "organisers", OrganisersController, only: [:index, :show] do
  #  resources "tags", TagsController, only: [:index]
  #  resources "events", EventsController, only: [:index]
  #end
end

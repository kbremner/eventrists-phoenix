defmodule Eventrist.EventsView do
  use Eventrist.Web, :view

  def action_text(:add) do
    "✓"
  end

  def action_text(:ignore) do
    "✗"
  end

  def action_path(:add, conn, event, redirect_path) do
    events_path(conn, :save_event, event.id, action: "added", redirect_path: redirect_path)
  end

  def action_path(:ignore, conn, event, redirect_path) do
    events_path(conn, :save_event, event.id, action: "ignored", redirect_path: redirect_path)
  end

  def is_in_past(time) do
    now = :calendar.universal_time
    dt = Ecto.DateTime.to_erl time

    now_secs = :calendar.datetime_to_gregorian_seconds now
    time_secs = :calendar.datetime_to_gregorian_seconds dt

    now_secs >= time_secs
  end

  def format_datetime(datetime) do
    {date, time} = Ecto.DateTime.to_erl datetime
    "#{format_erl_time time} on #{format_erl_date date}"
  end

  def format_from_to(from, to) do
    if to == nil do
      format_datetime from
    else
      {from_date, from_time} = Ecto.DateTime.to_erl from
      {to_date, to_time} = Ecto.DateTime.to_erl to
      if from_date == to_date do
        "#{format_erl_time from_time} - #{format_erl_time to_time} on #{format_erl_date to_date}"
      else
        "#{format_erl_time from_time} on #{format_erl_date from_date} - #{format_erl_time to_time} on #{format_erl_date to_date}"
      end
    end
  end

  defp format_erl_date({_year, month, day} = date) do
    dow = case :calendar.day_of_the_week date do
      1 -> "Mon"
      2 -> "Tues"
      3 -> "Wed"
      4 -> "Thurs"
      5 -> "Fri"
      6 -> "Sat"
      7 -> "Sun"
    end
    "#{dow}, #{format_day(day)} #{format_month(month)}"
  end

  defp format_erl_time({hours, mins, _secs}) do
    "#{format_digits hours}:#{format_digits mins}"
  end

  defp format_digits(digits) do
    case digits do
      x when x < 10 -> "0#{x}"
      x -> "#{x}"
    end
  end

  defp format_day(day) do
    units = case rem(day, 10) do
      1 -> "1st"
      2 -> "2nd"
      3 -> "3rd"
      x -> "#{x}th"
    end
    case div(day, 10) do
      0 -> "#{units}"
      1 -> "#{day}th"
      x -> "#{x}#{units}"
    end
  end

  defp format_month(month) do
    case month do
      1  -> "January"
      2  -> "February"
      3  -> "March"
      4  -> "April"
      5  -> "May"
      6  -> "June"
      7  -> "July"
      8  -> "August"
      9  -> "September"
      10 -> "October"
      11 -> "November"
      12 -> "December"
    end
  end

  def format_time_from_now(time) do
    {now_date, now_time} = :calendar.universal_time
    {time_date, time_time} = Ecto.DateTime.to_erl time

    now_days = :calendar.date_to_gregorian_days now_date
    time_days = :calendar.date_to_gregorian_days time_date

    now_secs = :calendar.time_to_seconds now_time
    time_secs = :calendar.time_to_seconds time_time

    case time_days - now_days do
      1                   -> "Starts tomorrow"
      -1                  -> "Started yesterday"
      days when days > 0  -> "Starts in #{days} days"
      days when days < 0  -> "Started #{-days} days ago"
      0                   ->
        case time_diff(time_secs, now_secs) do
          {0, 0, 0}                                       -> "Starting now"
          {1, mins, _} when mins == 30                    -> "Starts in an hour and a half"
          {1, mins, _} when mins < 30                     -> "Starts in about an hour"
          {-1, mins, _} when mins == -30                  -> "Started an hour and a half ago"
          {-1, mins, _} when mins > -30                   -> "Started about an hour ago"
          {hours, mins, _} when hours > 0 and mins > 30   -> "Starts in about #{hours+1} hours"
          {hours, mins, _} when hours < 0 and mins < -30  -> "Started about #{(-hours)+1} hours ago"
          {hours, mins, _} when hours > 0 and mins < 30   -> "Starts in about #{hours} hours"
          {hours, mins, _} when hours < 0 and mins > -30  -> "Started about #{-hours} hours ago"
          {hours, mins, _} when hours > 0 and mins == 30  -> "Starts in #{hours} and a half hours"
          {hours, mins, _} when hours < 0 and mins == -30 -> "Started #{-hours} and a half hours ago"
          {0, 1, _}                                       -> "Starts in a minute"
          {0, -1, _}                                      -> "Started a minute ago"
          {0, 30, _}                                      -> "Starts in half an hour"
          {0, -30, _}                                     -> "Started half an hour ago"
          {0, mins, _} when mins > 0                      -> "Starts in #{mins} minutes"
          {0, mins, _} when mins < 0                      -> "Started #{-mins} minutes ago"
          {0, 0, 1}                                       -> "Starts in a second"
          {0, 0, -1}                                      -> "Started a second ago"
          {0, 0, secs} when secs > 0                      -> "Starts in #{secs} seconds"
          {0, 0, secs} when secs < 0                      -> "Started #{-secs} seconds ago"
        end
    end
  end

  defp time_diff(a, b) do
    diff = a - b

    hours = div diff, (60 * 60)
    rem_hours = rem diff, (60 * 60)

    mins = div rem_hours, 60
    rem_mins = rem rem_hours, 60

    {hours, mins, rem_mins}
  end
end

defmodule Eventrist.Repo.Migrations.VenueDescriptionOptional do
  use Ecto.Migration

  def change do
    alter table(:venues) do
      modify :description, :text, null: true
    end
  end
end

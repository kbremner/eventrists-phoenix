defmodule Eventrist.Repo.Migrations.CreateCalendarKeyTable do
  use Ecto.Migration

  def change do
    create table(:calendar_keys) do
      add :user_id, references(:users), null: false
      add :key, :string, null: false
      timestamps
    end
  end
end

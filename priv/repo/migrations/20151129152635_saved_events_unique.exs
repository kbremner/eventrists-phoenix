defmodule Eventrist.Repo.Migrations.SavedEventsUnique do
  use Ecto.Migration

  def change do
    create unique_index(:user_saved_events, [:user_id, :event_id])
    create unique_index(:user_ignored_events, [:user_id, :event_id])
  end
end

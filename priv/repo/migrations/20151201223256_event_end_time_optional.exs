defmodule Eventrist.Repo.Migrations.EventEndTimeOptional do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :end_time, :datetime, null: true
    end
  end
end

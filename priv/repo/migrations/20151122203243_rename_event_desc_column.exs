defmodule Eventrist.Repo.Migrations.RenameEventDescColumn do
  use Ecto.Migration

  def change do
    rename table(:events), :desc, to: :description
  end
end

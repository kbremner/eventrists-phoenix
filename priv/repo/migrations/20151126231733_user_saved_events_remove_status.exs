defmodule Eventrist.Repo.Migrations.UserSavedEventsRemoveStatus do
  use Ecto.Migration

  def change do
    alter table(:user_saved_events) do
      remove :status
    end
  end
end

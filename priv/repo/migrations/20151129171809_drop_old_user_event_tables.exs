defmodule Eventrist.Repo.Migrations.DropOldUserEventTables do
  use Ecto.Migration

  def change do
    drop table :user_ignored_events
    drop table :user_saved_events
  end
end

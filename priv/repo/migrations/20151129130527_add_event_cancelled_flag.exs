defmodule Eventrist.Repo.Migrations.AddEventCancelledFlag do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :cancelled, :boolean, null: false, default: false
    end
  end
end

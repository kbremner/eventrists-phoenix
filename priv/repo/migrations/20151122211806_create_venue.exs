defmodule Eventrist.Repo.Migrations.CreateVenue do
  use Ecto.Migration

  def change do
    create table(:venues) do
      add :name, :string, null: false
      add :description, :text, null: false
      add :lat, :float, null: false
      add :lng, :float, null: false
      add :address, :text, null: false

      timestamps
    end

  end
end

defmodule Eventrist.Repo.Migrations.UserEmailUnique do
  use Ecto.Migration

  def change do
    create unique_index(:users, [:email])
    alter table(:users) do
      modify :email, :string, null: false
      modify :crypted_password, :string, null: false
    end
  end
end

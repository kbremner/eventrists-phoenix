defmodule Eventrist.Repo.Migrations.CreaeteUserIgnoredEventsTable do
  use Ecto.Migration

  def change do
    create table(:user_ignored_events) do
      add :user_id, references(:users), null: false
      add :event_id, references(:events), null: false
      timestamps
    end
  end
end

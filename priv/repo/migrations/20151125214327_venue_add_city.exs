defmodule Eventrist.Repo.Migrations.VenueAddCity do
  use Ecto.Migration

  def change do
    alter table(:venues) do
      add :city, :string, null: false
    end
  end
end

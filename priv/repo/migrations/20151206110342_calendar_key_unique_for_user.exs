defmodule Eventrist.Repo.Migrations.CalendarKeyUniqueForUser do
  use Ecto.Migration

  def change do
    create unique_index(:calendar_keys, [:user_id])
  end
end

defmodule Eventrist.Repo.Migrations.CreateEvent do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :source, :string
      add :source_id, :string
      add :title, :string
      add :desc, :text
      add :link, :string
      add :start_time, :datetime
      add :end_time, :datetime
      timestamps
    end

  end
end

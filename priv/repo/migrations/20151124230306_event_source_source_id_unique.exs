defmodule Eventrist.Repo.Migrations.EventSourceSourceIdUnique do
  use Ecto.Migration

  def change do
    create unique_index(:events, [:source, :source_id])
  end
end

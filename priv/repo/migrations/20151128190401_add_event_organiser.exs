defmodule Eventrist.Repo.Migrations.AddEventOrganiser do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :organiser, :string, null: false
    end
  end
end

defmodule Eventrist.Repo.Migrations.CreaeteUserEventsTables do
  use Ecto.Migration

  def change do
    create table(:user_saved_events) do
      add :user_id, references(:users), null: false
      add :event_id, references(:events), null: false
      add :status, :string, null: false
      timestamps
    end
  end
end

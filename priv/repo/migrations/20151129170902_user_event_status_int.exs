defmodule Eventrist.Repo.Migrations.UserEventStatusInt do
  use Ecto.Migration

  def change do
    alter table(:user_events) do
      remove :status
      add :status, :integer, null: false
    end
  end
end

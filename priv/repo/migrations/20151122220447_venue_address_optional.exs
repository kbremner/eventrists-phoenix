defmodule Eventrist.Repo.Migrations.VenueAddressOptional do
  use Ecto.Migration

  def change do
    alter table(:venues) do
      modify :address, :text, null: true
    end
  end
end

defmodule Eventrist.Repo.Migrations.LinkVenuesToEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :venue_id, references(:venues), null: false
    end
  end
end

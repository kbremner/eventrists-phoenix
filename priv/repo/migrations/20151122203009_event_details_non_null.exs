defmodule Eventrist.Repo.Migrations.EventDetailsNonNull do
  use Ecto.Migration

  def change do
    alter table(:events) do
      modify :source, :string, null: false
      modify :source_id, :string, null: false
      modify :title, :string, null: false
      modify :desc, :text, null: false
      modify :link, :string, null: false
      modify :start_time, :datetime, null: false
      modify :end_time, :datetime, null: false
    end
  end
end

defmodule Eventrist.Repo.Migrations.UserEventUnique do
  use Ecto.Migration

  def change do
    create unique_index(:user_events, [:user_id, :event_id])
  end
end

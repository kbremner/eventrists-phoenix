defmodule Collector.EventbriteCollector do
  @moduledoc """
  Collects events from an OpenACalendar instance
  Details of API here:
  http://docs.openacalendar.org/en/master/developers/core/
  """

  def collect([]) do
    case :httpc.request(:get, {'https://www.eventbriteapi.com/v3/users/me/', [{'Authorization', 'Bearer OZ3PJWAYVPUP6Y4HDJ2N'}]}, [], []) do
      {:ok, {{http_version, 200, reasonPhase}, headers, body}}  ->
        body
        |> parse_events
        #|> Stream.filter(&has_description(&1))
        #|> Stream.map(&handle_event(&1))
        #|> Enum.to_list
        |> IO.inspect
      {:error, reason} -> raise reason
    end
  end

  # parse the JSON response and pull out the events
  defp parse_events(events) do
    case Poison.Parser.parse!(events) do
       all_events -> all_events
    end
  end

  # filter out events that don't have a description
  defp has_description(event) do
    case event["description"] do
      nil -> false
      ""  -> false
      _   -> true
    end
  end

  # handle a single event
  defp handle_event(event) do
    %Eventrist.Event{
      :title        => event["summary"],
      :description  => event["description"],
      :link         => event["siteurl"],
      :source       => "eventbrite"
    }
  end
end

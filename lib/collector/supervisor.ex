defmodule Collector.Supervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end

  def init(_) do
    children = [
      # run collection every 5 mins
      worker(
        Collector.LoopedTask,
        [5 * 60 * 1000, &Collector.OtcCollector.collect(&1), "http://opentechcalendar.co.uk/api1/"],
        restart: :transient,
        id: :otc_collector)
      #worker(
      #  Collector.LoopedTask,
      #  [30 * 60 * 1000, &Collector.MeetupCollector.collect(&1), "https://api.meetup.com/"],
      #  restart: :transient,
      #  id: :meetup_collector)
    ]

    supervise(children, strategy: :one_for_one)
  end
end

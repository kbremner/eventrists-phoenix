defmodule Collector.OtcCollector do
  @moduledoc """
  Collects events from an OpenACalendar instance
  Details of API here:
  http://docs.openacalendar.org/en/master/developers/core/
  """

  @source "open_tech_calendar"
  @glasgow_area_slug "65"
  @glasgow_area_name "glasgow"
  @edin_area_slug "62"
  @edin_area_name "edinburgh"

  def collect(base_url) do
    collect(base_url, @glasgow_area_slug, @glasgow_area_name)
    |> Stream.concat(collect(base_url, @edin_area_slug, @edin_area_name))
    |> Stream.run
  end

  defp collect(base_url, area_code, area_name) do
    events_url = to_char_list("#{base_url}area/#{area_code}/events.json")
    case :httpc.request(events_url) do
      {:ok, {{_, 200, _}, _headers, body}}  ->
        body
        |> parse_events
        |> Stream.filter(&has_prop(&1, "description"))
        |> Stream.filter(&has_venue_location(&1))
        |> Stream.filter(&is_physical_event(&1))
        |> Stream.map(&split_summary_display(&1))
        |> Stream.filter(&has_prop(&1, "title"))
        |> Stream.filter(&has_prop(&1, "organiser"))
        |> Stream.map(&handle_event(&1, area_name))
      {:error, reason} -> raise reason
    end
  end

  # parse the JSON response and pull out the events
  defp parse_events(events) do
    case Poison.Parser.parse!(events) do
      %{"data" => all_events} -> all_events
    end
  end

  defp has_prop(event, prop) do
    case event[prop] do
      nil -> false
      ""  -> false
      _   -> true
    end
  end

  defp has_venue_location(event) do
    event["venue"]["lat"] != nil && event["venue"]["lng"] != nil
  end

  defp is_physical_event(event) do
    event["is_physical"]
  end

  defp split_summary_display(event) do
    summary = event["summary"]
    display = event["summaryDisplay"]

    # some not-very-nice stuff going on here...
    # OTC won't give us the organiser of an event,
    # so we need to infer it from the summary display
    {organiser, title} =
      case String.split(display, ": ") do
        [^summary] ->
          case String.split(display, " - ") do
            [^summary]   -> {summary, summary}
            [org, title] -> {org, title}
            _ -> {nil, nil}
          end
        [org, title] -> {org, title}
        _ -> {nil, nil}
      end

    Map.put(event, "organiser", organiser)
      |> Map.put("title", title)
  end

  # handle a single event
  defp handle_event(event, area_name) do
    Eventrist.Repo.transaction fn ->
      venue_changes = %{
        name:         event["venue"]["title"],
        description:  event["venue"]["description"],
        address:      event["venue"]["address"],
        lat:          parse_float!(event["venue"]["lat"]),
        lng:          parse_float!(event["venue"]["lng"]),
        city:         area_name
      }

      venue = case Eventrist.Repo.get_by(Eventrist.Venue, name: event["venue"]["title"]) do
        nil ->
          venue_changeset = Eventrist.Venue.changeset(%Eventrist.Venue{}, venue_changes)
          Eventrist.Repo.insert!(venue_changeset)
        x ->
          venue_changeset = Eventrist.Venue.changeset(x, venue_changes)
          Eventrist.Repo.update!(venue_changeset)
      end

      event_source_id = Integer.to_string(event["slug"])
      event_changes = %{
        title:        event["title"],
        description:  event["description"],
        link:         event["siteurl"],
        organiser:    event["organiser"],
        start_time:   parse_time!(event["start"]),
        end_time:     parse_time!(event["end"]),
        source_id:    event_source_id,
        source:       @source,
        venue_id:     venue.id,
        cancelled:    event["cancelled"] || event["deleted"]
      }

      # check if we've already got it
      event = Eventrist.Repo.get_by(Eventrist.Event, source: @source, source_id: event_source_id)

      if event != nil do
        event_changeset = Eventrist.Event.changeset(event, event_changes)
        Eventrist.Repo.update!(event_changeset)
      else
        # don't want to add a duplicate event, so need to check if there's
        # already one with the same organiser, start time and city
        q = Eventrist.Event.find_by_organiser_start_time_and_city(
          event_changes.organiser,
          event_changes.start_time,
          area_name)

        case Eventrist.Repo.one(q) do
          nil ->
            event_changeset = Eventrist.Event.changeset(%Eventrist.Event{}, event_changes)
            Eventrist.Repo.insert!(event_changeset)
          x   ->
            # already an event saved, prefer OpenTechCalendar events so write over it
            event_changeset = Eventrist.Event.changeset(x, event_changes)
            Eventrist.Repo.update!(event_changeset)
        end
      end
    end
  end

  defp parse_float!(float) do
    case Float.parse(float) do
      {f, _} -> f
      :error -> raise "failed to parse float " <> float
    end
  end

  defp parse_time!(time_map) do
    date = {time_map["yeartimezone"], time_map["monthtimezone"], time_map["daytimezone"]}
    time = {time_map["hourtimezone"], time_map["minutetimezone"], 0}
    case Ecto.DateTime.cast({date, time}) do
      {:ok, dt} -> dt
      {:error} -> raise "failed to parse datetime " <> time_map
    end
  end
end

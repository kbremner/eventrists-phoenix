defmodule Collector.LoopedTask do
  def start_link(sleep, func, args) do
    Task.start_link fn -> loop(sleep, func, args) end
  end

  defp loop(sleep, func, args) do
    func.(args)
    :timer.sleep(sleep)
    loop(sleep, func, args)
  end
end

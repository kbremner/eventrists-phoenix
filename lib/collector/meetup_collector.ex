defmodule Collector.MeetupCollector do
  @moduledoc """
  Collects events from an OpenACalendar instance
  Details of API here:
  http://docs.openacalendar.org/en/master/developers/core/
  """

  @source "meetup"
  @api_key "3648267c21322b613e21735633402"
  @country "gb"
  @glasgow_area_name "glasgow"
  @edin_area_name "edinburgh"
  @tech_category_id "34"

  def collect(base_url) do
    collect(base_url, @glasgow_area_name)
    |> Stream.concat(collect(base_url, @edin_area_name))
    |> Stream.run
  end

  defp collect(base_url, area_name) do
    events_url = to_char_list("#{base_url}2/open_events?&photo-host=public&country=#{@country}&city=#{area_name}&category=#{@tech_category_id}&key=#{@api_key}")
    case :httpc.request(events_url) do
      {:ok, {{_, 200, _}, _headers, body}}  ->
        body
        |> parse_events
        |> Stream.filter(&not_online(&1))
        |> Stream.filter(&has_venue_location(&1))
        |> Stream.map(&handle_event(&1, area_name))
      {:error, reason} -> raise reason
    end
  end

  # parse the JSON response and pull out the events
  defp parse_events(events) do
    case Poison.Parser.parse!(events) do
      %{"results" => all_events} -> all_events
    end
  end

  # filter out events that have a venue called "Online",
  # trying to remove virtual events
  defp not_online(event) do
    event["venue"]["name"] != "Online"
  end

  defp has_venue_location(event) do
    event["venue"]["lat"] != nil && event["venue"]["lon"] != nil
  end

  # handle a single event
  defp handle_event(event, area_name) do
    Eventrist.Repo.transaction fn ->
      venue_changes = %{
        name:         event["venue"]["name"],
        address:      event["venue"]["address_1"],
        lat:          event["venue"]["lat"],
        lng:          event["venue"]["lon"],
        city:         area_name
      }

      venue = case Eventrist.Repo.get_by(Eventrist.Venue, name: event["venue"]["name"]) do
        nil ->
          venue_changeset = Eventrist.Venue.changeset(%Eventrist.Venue{}, venue_changes)
          Eventrist.Repo.insert!(venue_changeset)
        x ->
          venue_changeset = Eventrist.Venue.changeset(x, venue_changes)
          Eventrist.Repo.update!(venue_changeset)
      end

      event_changes = %{
        title:        event["name"],
        description:  event["description"],
        link:         event["event_url"],
        organiser:    map_organiser(event["group"]["name"]),
        start_time:   parse_time!(event["time"]),
        source_id:    event["id"],
        source:       @source,
        venue_id:     venue.id,
        cancelled:    false
      }

      # don't want to add a duplicate, so check if organiser has another event
      # in the same city with the same start time
      q = Eventrist.Event.find_by_organiser_start_time_and_city(
        event_changes.organiser,
        event_changes.start_time,
        area_name)

      event = case Eventrist.Repo.one(q) do
        nil ->
          event_changeset = Eventrist.Event.changeset(%Eventrist.Event{}, event_changes)
          Eventrist.Repo.insert!(event_changeset)
        x   ->
          # found an event, only want to update it if it's our event
          if x.source == @source do
            event_changeset = Eventrist.Event.changeset(x, event_changes)
            Eventrist.Repo.update!(event_changeset)
          end
      end
    end
  end

  defp parse_time!(millis) do
    base_date = :calendar.datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}})
    seconds = base_date + div(millis,1000)
    datetime = :calendar.gregorian_seconds_to_datetime(seconds)
    case Ecto.DateTime.cast(datetime) do
      {:ok, dt} -> dt
      {:error} -> raise "failed to parse datetime " <> millis
    end
  end

  defp map_organiser("Edinburgh User Experience Meetup") do
    "Edinburgh UX meetup"
  end

  defp map_organiser(organiser) do
    organiser
  end
end

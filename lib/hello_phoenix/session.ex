defmodule Eventrist.Session do
  import Plug.Conn, only: [get_session: 2]

  def logged_in?(conn) do
    get_session(conn, :current_user) != nil
  end
end
